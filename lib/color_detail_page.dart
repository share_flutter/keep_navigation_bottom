import 'package:flutter/material.dart';
import 'package:flutter_fixed_navigation/test_view.dart';

class ColorDetailPage extends StatelessWidget {
  ColorDetailPage({this.color, this.title, this.materialIndex: 500});
  final MaterialColor color;
  final String title;
  final int materialIndex;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: color,
        title: Text(
          '$title[$materialIndex]',
        ),
      ),
      body: Column(
        children: [
          Text("Hello world"),
          Text("Hello world"),
          Text("Hello world"),
          Text("Hello world"),
          TextButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => TestView()));
              },
              child: Text("Save"))
        ],
      ),
    );
  }
}
